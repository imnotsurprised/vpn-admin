<?php

namespace models;

use PDO;

class Users extends \models\Main
{
    public function create($data)
    {
        try
        {
            $sql = "
                INSERT INTO users
                    (`name`, `email`, `company_id`)
                VALUES
                    (:name, :email, :company_id)";
            
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
            $stmt->bindParam(':company_id', $data['company'], PDO::PARAM_INT);
            
            return $stmt->execute() ? true : false;
        } 
        catch (PDOException $ex) 
        {
            return $ex->getMessage();
        }
    }
    
    public function delete($id)
    {
        try
        {
            $sql = "DELETE FROM users WHERE id = :id LIMIT 1";
        
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            
            return $stmt->execute() ? true : false;
        }
        catch (PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
    
    public function update($data)
    {
        try
        {
            $sql = "UPDATE users SET `name` = :name, `email` = :email, `company_id` = :company WHERE `id` = :id LIMIT 1";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
            $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindParam(':email', $data['email'], PDO::PARAM_STR);
            $stmt->bindParam(':company', $data['company'], PDO::PARAM_INT);
            $stmt->execute();
            
            return ($stmt->rowCount() > 0) ? true : false;
        } 
        catch (PDOException $ex)
        {
            print_r($ex->getMessage());
            return $ex->getMessage();
        }
    }
    
    public function getAll()
    {
        try
        {
            $sql = "SELECT 
                        U.`id`, U.`name`, U.email, C.`id` as `company_id`, C.`name` as `company_name`
                    FROM
                        users as U,
                        companies as C
                    WHERE 
                        C.id = U.company_id
                    ";

            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        }
        catch(PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
    
    public function getByCompanyId($companyId)
    {
        try
        {
            $sql = "SELECT 
                        U.`id`, U.`name`, U.email, C.`id` as `company_id`, C.`name` as `company_name`
                    FROM
                        users as U,
                        companies as C
                    WHERE 
                        C.id = U.company_id AND
                        C.id = :company_id
                    ";

            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, PDO::PARAM_INT);
            $stmt->execute();

            return $stmt->fetchAll();
        }
        catch(PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
}