<?php

namespace models;

use PDO;

class Abusers extends \models\Main
{
    private $companiesModel;
    
    public function __construct() 
    {
        parent::__construct();
        
        $this->companiesModel = new \models\Companies();
    }
    
    public function create($data)
    {
        try
        {
            $sql = "
                INSERT INTO transfer_logs
                    (`user_id`, `date`, `resource`, `transferred`)
                VALUES
                    (:user_id, :date, :resource, :transferred)";
            
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':user_id', $data['user_id'], PDO::PARAM_INT);
            $stmt->bindParam(':date', $data['date'], PDO::PARAM_STR);
            $stmt->bindParam(':resource', $data['resource'], PDO::PARAM_STR);
            $stmt->bindParam(':transferred', $data['transferred'], PDO::PARAM_INT);
            
            return $stmt->execute() ? true : false;
            
        } 
        catch (PDOException $ex) 
        {
            return $ex->getMessage();
        }
    }
    
    public function getDataByMonth($month)
    {
        $usersTransfers = $this->getUserTransfersByMonth($month);
        $usersTransfers = $this->sortByCompany($usersTransfers);
        
        $companiesList = $this->companiesModel->getAll();
        
        $outputData = array();
        
        foreach ($companiesList as $company)
        {
            if (isset($usersTransfers[$company['id']]) && 
                $company['quota'] < $usersTransfers[$company['id']]['transfers'])
            {
                $outputData[] = array(
                    'name' => $company['name'],
                    'quota' => $company['quota'],
                    'used' => $usersTransfers[$company['id']]['transfers'],
                    'usersUsed' => $usersTransfers[$company['id']]['users']
                );
            }
        }
        
        return $outputData;
    }
    
    private function getUserTransfersByMonth($month)
    {
        $lastMonthDay =  cal_days_in_month(CAL_GREGORIAN, $month, date('Y'));
        
        $start = date('Y') . "-$month-01 00:00:00";
        $end = date('Y') . "-$month-$lastMonthDay 23:59:59";
        
        try
        {
            $sql = "
                SELECT 
                    U.name, U.email,  U.company_id, sum(TL.transferred) as user_transfer
                FROM
                    users as U,
                    transfer_logs as TL
                WHERE 
                    TL.date BETWEEN :start AND :end AND U.id = TL.user_id
                GROUP BY
                    TL.user_id";
            
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':start', $start);
            $stmt->bindParam(':end', $end);
            $stmt->execute();
            
            return $stmt->fetchAll();
        } 
        catch (PDOException $ex)
        {
            return $ex-getMessage();
        }
    }
    
    private function sortByCompany($array)
    {
        $result = array();
        
        foreach ($array as $arr)
        {
            $result[$arr['company_id']]['users'][] = array(
                'name' => $arr['name'],
                'email' => $arr['email'],
                'used' => $arr['user_transfer']
            );
            
            $result[$arr['company_id']]['transfers'][] = $arr['user_transfer'];
        }
        
        foreach ($result as &$data)
        {
            $total = 0;
            foreach ($data['transfers'] as $transfer) 
            {
                $total += $transfer;
            }
            $data['transfers'] = $total;
        }
        
        return $result;
    }
}