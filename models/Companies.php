<?php

namespace models;

use PDO;

class Companies extends \models\Main
{
    public function create($data)
    {
        try
        {
            $sql = "
                INSERT INTO companies
                    (`name`, `quota`)
                VALUES
                    (:name, :quota)";
            
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindParam(':quota', $data['quota'], PDO::PARAM_INT);
            
            return $stmt->execute() ? true : false;
        } 
        catch (PDOException $ex) 
        {
            return $ex->getMessage();
        }
    }
    
    public function delete($id)
    {
        try
        {
            $sql = "DELETE FROM companies WHERE id = :id LIMIT 1";
        
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            
            return $stmt->execute() ? true : false;
        }
        catch (PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
    
    public function update($data)
    {
        try
        {
            $sql = "UPDATE companies SET `name` = :name, `quota` = :quota WHERE `id` = :id LIMIT 1";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
            $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindParam(':quota', $data['quota'], PDO::PARAM_INT);
            $stmt->execute();
            
            return ($stmt->rowCount() > 0) ? true : false;
        } 
        catch (PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
    
    public function getAll()
    {
        try
        {
            $sql = "SELECT `id`, `name`, `quota` FROM companies";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        }
        catch(PDOException $ex)
        {
            return $ex->getMessage();
        }
    }
}