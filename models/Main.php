<?php

namespace models;

use PDO;

class Main
{
    protected $dbh;
    
    public function __construct() 
    {
        try
        {
            $opt = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );
            
            $this->dbh = new PDO('mysql:host=' . DB_HOST . ';dbname='. DB_NAME, DB_USER, DB_PASS, $opt);
        } 
        catch (PDOException $ex) 
        {
            die ('Error connect to host: ' . $e->getMessage());
        }
    }
}