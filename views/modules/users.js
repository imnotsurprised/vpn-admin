Core.modules.Users = {
    validateRules: {
        inputName: {
            required: true,
            minlength: 3,
            maxlength:30
        },
        inputEmail: {
            required: true,
            email: true
        },
        selectCompany: "required"
    },
    
    init: function() {
        var _self = this;
        
        $('#container').load('views/templates/users/main.html', function() {
            var req = Core.modules.Helper.ajax('GET', 'users/get');
            req.success(function(response) {
                if (response.data.length > 0) {
                    $.each(response.data, function(key, user) {
                        var editButton = $('<button class="btn btn-info btn-sm">edit</button>');
                        editButton.click(function() {
                            _self.edit(user);
                        });
                        
                        var deleteButton = $('<button class="btn btn-danger btn-sm">delete</button>');
                        deleteButton.click(function() {
                            _self.delete(user.id);
                        });
                        deleteButton.css('margin-left', '4px');
                        
                        $('#users').find('tbody').append(
                            $('<tr>')
                                .append($('<td>').append(user.name))
                                .append($('<td>').append(user.email))
                                .append($('<td>').append(user.company_name))
                                .append($('<td>').append(editButton).append(deleteButton))
                        );
                    });
                }
                
                $('#users').dataTable();
            });
            req.error(function(xhr) {
                Core.modules.Helper.notice(xhr.responseJSON.error, 'red');
            });
        });
    },
    
    delete: function(id) {
        if (confirm('Your you really want to delete this user?')) {
            var req = Core.modules.Helper.ajax('DELETE', 'users/delete/' + id);
            req.success(function(response) {
                Core.modules.Helper.notice(response.data, 'green');
                Core.modules.Users.init();
            });
            req.error(function(xhr) {
                Core.modules.Helper.notice(xhr.responseJSON.error, 'red');
            });
        }
    },
    
    add: function() {
        var _self = this;
        
        var req = Core.modules.Helper.ajax('get', 'companies/get');
        req.success(function(response) {
            if (response.data.length == 0) {
                Core.modules.Helper.notice('Companies not found', 'red');
                return;
            }
            
            Core.modules.Helper.modal({title: 'Add new user', width: 500});
            
            $('#dialog-content').load('views/templates/users/add.html', function() {
                $.each(response.data, function(key, company) {
                    $('#selectCompany').append($('<option/>').html(company.name).val(company.id));
                });
                
                $('#user-form').validate({
                    rules: _self.validateRules,
                    submitHandler: function(form) {
                        _self.submitAddForm();
                    }
                });
                
                Core.modules.Helper.modalAction('open');
            });
            
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    },
    
    submitAddForm: function() {
        var name = $('#inputName').val();
        var email = $('#inputEmail').val();
        var company = $('#selectCompany option:selected').val();
        
        var req = Core.modules.Helper.ajax('POST', 'users/post', {name: name, email: email, company: company});
        req.success(function(response) {
            Core.modules.Helper.modalAction('close');
            Core.modules.Helper.notice(response.data, 'green');
            Core.modules.Users.init();
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    },
    
    edit: function(user) {
        var _self = this;
        
        var req = Core.modules.Helper.ajax('get', 'companies/get');
        req.success(function(response) {
            Core.modules.Helper.modal({title: 'Edit user', width: 500});
            $('#dialog-content').load('views/templates/users/add.html', function() {
                $('#inputName').val(user.name);
                $('#inputEmail').val(user.email);
                
                $.each(response.data, function(key, company) {
                    $('#selectCompany').append($('<option/>').html(company.name).val(company.id));
                });
                $('#selectCompany option[value="'+ user.company_id +'"]').prop('selected', true);

                $('#user-form').validate({
                    rules: _self.validateRules,
                    submitHandler: function(form) {
                        _self.submitEditForm(user);
                    }
                });
                
                Core.modules.Helper.modalAction('open');
            })
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    },
    
    submitEditForm: function(user) {
        var name = $('#inputName').val();
        var email = $('#inputEmail').val();
        var company = $('#selectCompany option:selected').val();
        
        if (name == user.name && email == user.email && company == user.company_id ) {
            Core.modules.Helper.modalAction('close');
            return;
        }
        
        var data = {
            id: user.id,
            name: name,
            email: email,
            company: company
        };
        var req = Core.modules.Helper.ajax('PUT', 'users/put', data);
        req.success(function(response) {
            Core.modules.Helper.modalAction('close');
            Core.modules.Helper.notice(response.data, 'green');
            Core.modules.Users.init();
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    }
};