Core.modules.Companies = {
    validateRules: {
        inputName: {
            required: true,
            minlength:3,
            maxlength:30
        },
        inputQuota: {
            required: true,
            number: true,
            min: 1,
            maxlength:20
        }
    },
    
    init: function() {
        var _self = this;
        
        $('#container').load('views/templates/companies/main.html', function() {
            var req = Core.modules.Helper.ajax('GET', 'companies/get');
            req.success(function(response) {
                if (response.data.length > 0) {
                    $.each(response.data, function(key, company) {
                        var editButton = $('<button class="btn btn-info btn-sm">edit</button>');
                        editButton.click(function() {
                            _self.edit(company);
                        });
                        
                        var deleteButton = $('<button class="btn btn-danger btn-sm">delete</button>');
                        deleteButton.click(function() {
                            _self.delete(company.id);
                        });
                        deleteButton.css('margin-left', '4px');
                        
                        $('#companies').find('tbody').append(
                            $('<tr>')
                                .append($('<td>').append(company.name))
                                .append($('<td>').append(company.quota))
                                .append($('<td>').append(editButton).append(deleteButton))
                        );
                    });
                }
                
                $('#companies').dataTable();
            });
        });
    },
    
    add: function() {
        var _self = this;
        
        Core.modules.Helper.modal({title: 'Add new company'});
        
        $('#dialog-content').load('views/templates/companies/add.html', function() {
            $('#company-form').validate({
                rules: _self.validateRules,
                submitHandler: function(form) {
                    _self.submitAddForm();
                }
            });
            
            Core.modules.Helper.modalAction('open');
        });
    },
    
    submitAddForm: function() {
        var name = $.trim($('#inputName').val());
        var quota = $.trim($('#inputQuota').val());
        
        var req = Core.modules.Helper.ajax('POST', 'companies/post', {name: name, quota: quota});
        req.success(function(response) {
            Core.modules.Helper.modalAction('close');
            Core.modules.Helper.notice(response.data, 'green');
            Core.modules.Companies.init();
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    },
    
    edit: function(company) {
        var _self = this;
        
        Core.modules.Helper.modal({title: 'Edit company'});
            
        $('#dialog-content').load('views/templates/companies/add.html', function() {
            $('#inputName').val(company.name);
            $('#inputQuota').val(company.quota);
            
            $('#company-form').validate({
                rules: _self.validateRules,
                submitHandler: function(form) {
                    _self.submitEditForm(company);
                }
            });
            
            Core.modules.Helper.modalAction('open');
        });
    },
    
    submitEditForm: function(company) {
        var name = $.trim($('#inputName').val());
        var quota = $.trim($('#inputQuota').val());
        
        if (name == company.name && quota == company.quota) {
            Core.modules.Helper.modalAction('close');
            return;
        }
        
        var data = {
            id: company.id,
            name: name,
            quota: quota
        };
        var req = Core.modules.Helper.ajax('PUT', 'companies/put', data);
        req.success(function(response) {
            Core.modules.Helper.modalAction('close');
            Core.modules.Helper.notice(response.data, 'green');
            Core.modules.Companies.init();
        });
        req.error(function(response) {
            Core.modules.Helper.notice(response.responseJSON.error, 'red');
        });
    },
    
    delete: function(id) {
        if (confirm('Your you really want to delete this company?')) {
            var req = Core.modules.Helper.ajax('DELETE', 'companies/delete/' + id);
            req.success(function(response) {
                Core.modules.Helper.notice(response.data, 'green');
                Core.modules.Companies.init();
            });
            req.error(function(xhr) {
                Core.modules.Helper.notice(xhr.responseJSON.error, 'red');
            });
        }
    }
}