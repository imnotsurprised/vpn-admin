Core.modules.Abusers = {
    init: function() {
        $('#container').load('views/templates/abusers/main.html', function() {
            $('.input-group.date').datepicker({
                format: "mm/yyyy",
                minViewMode: 1,
                todayBtn: 'linked',
                autoclose: true
            });
            
            $('#abusers').dataTable();
        });
    },
    
    generateData: function() {
        var req = Core.modules.Helper.ajax('POST', 'abusers/generate');
        req.success(function(response) {
            Core.modules.Helper.notice(response.data, 'green');
        });
        req.error(function(xhr) {
            Core.modules.notice(xhr.responseJSON.error, 'red');
        });
    },
    
    showReport: function() {        
        var month = $(".input-group.date").data('datepicker').getFormattedDate('mm');
        
        if ('' == month) {
            Core.modules.Helper.notice('Please specify month', 'red');
            return;
        }
        
        $("#abusers").dataTable().fnDestroy();
        $('#abusers').find('tbody').empty();
        
        var req = Core.modules.Helper.ajax('GET', 'abusers/report/' + month);
        req.success(function(response) {
            if (response.data.length > 0) {
                $.each(response.data, function(key, company) {
                    var viewButton = $('<button class="btn btn-info btn-sm">view</button>');
                    viewButton.click(function() {
                        Core.modules.Abusers.viewUsersByCompany(company.usersUsed);
                    });
                    $('#abusers').find('tbody').append(
                        $('<tr>')
                            .append($('<td>').append(company.name))
                            .append($('<td>').append(company.used))
                            .append($('<td>').append(company.quota))
                            .append($('<td>').append(viewButton))
                    );
                });
            }
            else {
                Core.modules.Helper.notice('Data not found for current period', 'blue');
            }
        });
        req.error(function(xhr) {
            Core.modules.Helper.notice(xhr.responseJSON.error, 'red');
        });
        
        $('#abusers').dataTable({"aaSorting": [[ 1, "desc" ]]});
    },
    
    viewUsersByCompany: function(usersData) {
        Core.modules.Helper.modal({title: 'View users', width: 800});
        
        $('#dialog-content').load('views/templates/abusers/view-users.html', function() {
            $.each(usersData, function(key, user) {
                $('#abusers-users').find('tbody').append(
                    $('<tr>')
                        .append($('<td>').append(user.name))
                        .append($('<td>').append(user.email))
                        .append($('<td>').append(user.used))
                );
            });
            
            $('#abusers-users').dataTable({"aaSorting": [[ 2, "desc" ]]});
            
            Core.modules.Helper.modalAction('open');
        });
    }
}