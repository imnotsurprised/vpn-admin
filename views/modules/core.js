var Core = {
    init: function() {
        this.modules.Users.init();
        this.prepareMenu();
    },
    
    prepareMenu: function() {
        $('.nav a').click(function() {
            $('.nav li').removeClass('current');
            $(this).parent().addClass('current');
        });
    }
};

Core.modules = {};