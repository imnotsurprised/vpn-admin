<?php

namespace classes;

use Slim\Slim;

final class Router
{
    public function registerRoutes(Slim $core)
    {
        $core->get('/', function() use ( $core ){
            $core->render('index.html');
        });
        
        
        $core->map('/:controller/:method(/)(:params)', function($controller, $method, $params = null) use ( $core ){
            
            $core->response->header('Content-Type', 'application/json; charset=utf-8');
            
            $controller = ucfirst($controller);
            $controller = "\\controllers\\$controller";
            
            if (class_exists($controller))
            {
                $controllerObj = new $controller;
                
                if (method_exists($controllerObj, $method))
                {
                    $controllerObj->$method($params);
                }
                else
                {
                    $message = json_encode(array('error' => 'Method not found'));
                    $core->response->status(400);
                    $core->response->body($message);
                }
            }
            else
            {
                $message = json_encode(array('error' => 'Controller not found'));
                $core->response->status(400);
                $core->response->body($message);
            }
            
        })->via('GET', 'POST', 'PUT', 'DELETE');
    }
}