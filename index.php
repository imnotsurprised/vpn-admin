<?php

require_once 'config.php';
require_once 'Slim/Slim.php';
require_once 'libs/Faker/src/autoload.php';

\Slim\Slim::registerAutoloader();

$config = array(
    'mode' => CORE_MODE,
    'debug' => CORE_DEBUG,
    'templates.path' => './views/templates'
);

$core = new \Slim\Slim($config);

$routes = new \classes\Router();

/**
 * check http requests
 */
$routes->registerRoutes($core);

$core->run();