<?php

namespace controllers;

class Main
{
    /**
     * @return \Slim\Slim
     */
    protected function getApp() 
    {
        return \Slim\Slim::getInstance();
    }
    
    protected function response($status, $message)
    {
        $message = ($status > 200) ? array('error' => $message) : array('data' => $message);
        
        $this->getApp()->response->status($status);
        $this->getApp()->response->header('Content-Type', 'application/json; charset=utf-8');
        $this->getApp()->response->body(json_encode($message));
    }
}