<?php

namespace controllers;

class Users extends \controllers\Main
{
    private $usersModel;
    
    public function __construct()
    {
        $this->usersModel = new \models\Users();
    }
    
    public function get()
    {
        $result = $this->usersModel->getAll();
        
        $this->response(200, $result);
    }
    
    public function post()
    {
        $data = $this->getApp()->request->post();
        
        $result = $this->usersModel->create($data);
        
        if ($result)
        {
            $this->response(200, 'User added successfully');
            return;
        }
        
        $this->response(400, 'Error from add new user');
    }
    
    public function delete($id)
    {
        $result = $this->usersModel->delete($id);
        
        if ($result)
        {
            $this->response(200, 'User deleted successfully');
            return;
        }
        
        $this->response(400, 'Error from delete user');
    }
    
    public function put()
    {
        $data = $this->getApp()->request->put();
        
        $result = $this->usersModel->update($data);
        
        if ($result)
        {
            $this->response(200, 'User updated successfully');
            return;
        }
        
        $this->response(400, 'Error from update user');
    }
}