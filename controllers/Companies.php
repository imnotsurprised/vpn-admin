<?php

namespace controllers;

class Companies extends \controllers\Main
{
    private $companiesModel;
    private $usersModel;
    
    public function __construct()
    {
        $this->companiesModel = new \models\Companies();
        $this->usersModel = new \models\Users();
    }
    
    public function get()
    {
        $result = $this->companiesModel->getAll();
        
        $this->response(200, $result);
    }
    
    public function post()
    {
        $data = $this->getApp()->request->post();
        
        $result = $this->companiesModel->create($data);
        
        if ($result)
        {
            $this->response(200, 'Company added successfully');
            return;
        }
        
        $this->response(400, 'Error from add new company');
    }
    
    public function delete($id)
    {
        $usersByCompany = $this->usersModel->getByCompanyId($id);
        
        if (count($usersByCompany) > 0)
        {
            $this->response(400, 'Company assigned to user');
            return;
        }
        
        $result = $this->companiesModel->delete($id);
        
        if ($result)
        {
            $this->response(200, 'Company deleted successfully');
            return;
        }
        
        $this->response(400, 'Error from delete company');
    }
    
    public function put()
    {
        $data = $this->getApp()->request->put();
        
        $result = $this->companiesModel->update($data);
        
        if ($result)
        {
            $this->response(200, 'Company updated successfully');
            return;
        }
        
        $this->response(400, 'Error from update company'); 
    }
}