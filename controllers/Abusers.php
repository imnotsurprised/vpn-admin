<?php

namespace controllers;

use Faker;

class Abusers extends \controllers\Main
{
    private $abusersModel;
    private $usersModel;
    private $faker;
    
    public function __construct()
    {
        $this->abusersModel = new \models\Abusers();
        $this->usersModel = new \models\Users();
        $this->faker = Faker\Factory::create();
    }
    
    public function report($month)
    {
        $data = $this->abusersModel->getDataByMonth($month);
        
        $this->response(200, $data);
    }
    
    public function generate()
    {
        $usersList = $this->usersModel->getAll();
        
        if (empty($usersList))
        {
            $this->response(400, 'Please add user');
            return;
        }
        
        foreach ($usersList as $user)
        {
            $length = rand(50, 500);
            
            for ($i = 0; $i <= $length; $i++)
            {
                $data = array();
                $data['user_id'] = $user['id'];
                $data['date'] = $this->faker->dateTimeBetween('now', '+6 months')->format('Y-m-d H:i:s');
                $data['resource'] = $this->faker->url;
                $data['transferred'] = $this->faker->biasedNumberBetween(100, 100000);
                
                $this->abusersModel->create($data);
            }
        }
        
        $this->response(200, 'Data generated successfully');
    }
}