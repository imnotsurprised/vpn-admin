<?php

define('DEBUG', false);

if (DEBUG)
{
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);
    define('CORE_MODE', 'development');
    define('CORE_DEBUG', true);
}
else
{
    define('CORE_MODE', 'production');
    define('CORE_DEBUG', false);
}

define('DB_HOST', 'localhost');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', 'vpn-admin');